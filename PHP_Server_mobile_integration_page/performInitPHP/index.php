<?php

// ##############################
// ### TRANSACTION PARAMETERS ###
// ##############################

// Private Merchant KEY: Keep it safe!
$key =  "2503256198873034";//'1365613330086542';

$payzen_bacnkend_url = 'https://secure.payzen.co.in/vads-payment/entry.silentInit.a';
$_POST['amount'] = "222";
// prepare payzen reaquest parameters
$param['vads_site_id'] = "23475679";//'22322173'; //Merchant Shop ID
$param['vads_amount'] = str_replace(',', '.', $_POST['amount']); //transaction amount in paise
$param['vads_cust_email'] = "hancquartlouis@gmail.com"; //$_POST['email']; //customer email
$param['vads_order_id'] =  rand(10000, 100000) . '';  //order ID: generated for testing purposes

// Mandatory Variables
$param['vads_currency'] = "356"; //INR
$param['vads_ctx_mode'] = "PRODUCTION";
$param['vads_page_action'] = "PAYMENT";
$param['vads_action_mode'] = "INTERACTIVE";
$param['vads_payment_config'] = "SINGLE";
$param['vads_version'] = "V2";
$param['vads_trans_date'] = gmdate("YmdHis");
$param['vads_trans_id'] = gmdate("His");
$param['vads_return_mode'] = 'GET';
$param['vads_url_return'] = 'http://webview.return';
$param['vads_url_success'] = 'http://webview.success';
$param['vads_url_refused'] = 'http://webview.refused';
$param['vads_url_cancel'] = 'http://webview.cancel';
$param['vads_url_error'] = 'http://webview.error';
$param['vads_language'] = "en";



// ###########################
// ### TRANSACTION SIGNING ###
// ###########################

ksort($param); //parameters alphabetic sort
$signature_content = "";
foreach ($param as $name => $param_value) {
    $signature_content .= $param_value . "+";
}

// we add the Merchant key in the end
$signature_content .= $key;
// Then generate a sha1 on  all of it
$param['signature'] = sha1($signature_content);



// ###########################
// ### DEBUG VERIFICATIONS ###
// ###########################
$amount = $_POST['amount'];
if (empty($amount)) {
    echo('<p id="msgAttention"> Warning:</p>
                      <p id="msgError"><b>Purchase amount</b> is empty, press the button below and try again.</p>');
    echo '<form><input type="button" value="New Order" OnClick="window.location.href=\'http://demo.lyra-network.com/flipkart/index0.php\'"></form>';
    exit();
}



// ##############################
// ### CALL TO PAYZEN BACKEND ###
// ##############################

//prepare curl options
$curl_connection = curl_init($payzen_bacnkend_url);
curl_setopt($curl_connection, CURLOPT_CONNECTTIMEOUT, 15);
curl_setopt($curl_connection, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)");
curl_setopt($curl_connection, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl_connection, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($curl_connection, CURLOPT_FOLLOWLOCATION, 1);

// put the prepared params in an array to be embedded in the curl request
foreach ($param as $key => $value) {
    $post_items[] = $key . '=' . $value;
}

$post_string = implode('&', $post_items);
curl_setopt($curl_connection, CURLOPT_POSTFIELDS, $post_string);

//curl request to th payzen backend to get  the URL the webview of the app should target
$result = curl_exec($curl_connection);

//we print the result of the request so that the mobile App can retrieve the provided URL for the payment
print_r($result);

curl_close($curl_connection);




